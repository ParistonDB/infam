var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var users = require('./models/user');
var categories = require('./models/category');
var rss = require('./models/rss');
var fs = require('fs');

function checkAuth(req, res, next) {
  if(!req.session.user) {
    res.render('index.html');
  } else {
    next();
  }
}

/* GET home page. */
router.get('/', checkAuth, function(req, res, next) {
  res.render('dashboard.html', { title: 'Dashboard' });
});

router.post('/settings/editPhoto', function(req, res, next) {
  /*
  // get the temporary location of the file
  var tmp_path = req.body.photoName;
  // set where the file should actually exists - in this case it is in the "images" directory
  var target_path = './public/profilePhotos/' + req.body.photoName;
  // move the file from the temporary location to the intended location
  fs.rename(tmp_path, target_path, function(err) {
      if (err) throw err;
      // delete the temporary file, so that the explicitly set temporary upload dir does not get filled with unwanted files
      fs.unlink(tmp_path, function() {
          if (err) throw err;
          //res.send('File uploaded to: ' + target_path + ' - ' + req.files.thumbnail.size + ' bytes');
      });
  });
  */
  
  users.findOneAndUpdate({
    _id: req.body.userID
  }, {
    photo: req.body.photoName
  },function(error, result) {
    if(error) console.log(error);
    else {
      if(result !== null) {
        res.send("OK");
      }
    }
  });
});

router.post('/settings/editPassword', function(req, res, next) {
  users.findOneAndUpdate({
    _id: req.body.userID,
    password: req.body.oldPassword
  }, {
    password: req.body.newPassword
  },function(error, result) {
    if(error) console.log(error);
    else {
      if(result !== null) {
        res.send("OK");
      }
    }
  });
});

router.post('/settings/articles', function(req, res, next) {
  var content;
  var parseString = require('xml2js').parseString;
  parseString(req.body.tresc, function (err, result) {
    res.json(result.rss.channel[0].item.slice(0, 15));
  });
});

router.post('/settings/addMainRSS', function(req, res, next) {
  console.log(req.body);
    users.findOneAndUpdate({
      _id: req.body.userID
    }, {
      mainRSS: req.body.rssID
    }, function(error, result) {
      if(error) console.log(error);
      else {
        res.send("OK");
      }
    });
});

router.post('/settings/addRSS', function(req, res, next) {
  var rss_toAdd = new rss();
  rss_toAdd._id = mongoose.Types.ObjectId();
  rss_toAdd.url = req.body.url;
  rss_toAdd.ownerId = req.session.user;
  rss_toAdd.name = req.body.name;
  rss_toAdd.categoryId = req.body.categoryId;

  rss.findOne({
    url: req.body.url,
    ownerId: req.session.user
  }, function(error, result) {
     if(error) console.log(error);
     else {
       if(result === null) {
         rss_toAdd.save(function(error) {
           if(error) console.log(error);
           else {
             users.findOne({
               _id: req.session.user
             }, function (err, user) {
                if (err) console.log(err);
                if(user) {
                  user.rss.push(rss_toAdd);
                  user.save();
                }
             });

             categories.findOne({
               _id: req.body.categoryId
             }, function (err, category) {
                if (err) console.log(err);
                if(category) {
                  category.rss.push(rss_toAdd);
                  category.save();
                }
             });
           }
         });
       }
     }
  });
});

router.post('/settings/editRSS', function(req, res, next) {
    rss.findOneAndUpdate({
      _id: req.body.id
    }, {
      name: req.body.newName
    },function(error, result) {
      if(error) console.log(error);
      else {
        res.send("OK");
      }
    });
});

router.post('/settings/removeRSS', function(req, res, next) {
    rss.findOne({
      _id: req.body.id
    }, function(error, result) {
      if(error) console.log(error);
      else {
        if(result !== null) {
          result.remove();
          res.send("OK");
        }
      }
    });
});

router.post('/settings/addCategory', function(req, res, next) {
  var category_toAdd = new categories();
  category_toAdd._id = mongoose.Types.ObjectId();
  category_toAdd.name = req.body.categoryName;
  category_toAdd.ownerId = req.session.user;

  categories.findOne({
    name: req.body.categoryName,
    ownerId: req.session.user
  }, function(error, result) {
     if(error) console.log(error);
     else {
       if(result === null) {
         category_toAdd.save(function(error) {
           if(error) console.log(error);
           else {
             console.log("Dodano kategorie: " + category_toAdd);
             users.findOne({
               _id: req.session.user
             },
                function (err, userek) {
                if (err) console.log(err);
                if(userek) {
                  console.log(userek);
                  userek.categories.push(category_toAdd);
                }
                userek.save();
                res.send('Ok');
             });
           }
         });
       }
     }
  });
});

router.post('/settings/editCategory', function(req, res, next) {
    categories.findOneAndUpdate({
      _id: req.body.categoryId
    }, {
      name: req.body.newName
    },function(error, result) {
      if(error) console.log(error);
      else {
        res.send("OK");
      }
    });
});

router.post('/settings/removeCategory', function(req, res, next) {
    categories.findOne({
      _id: req.body.categoryId
    }, function(error, result) {
      if(error) console.log(error);
      else {
        if(result !== null) {
          result.remove();
          res.send("OK");
        }
      }
    });
});

router.post('/settings/getCategoriesByUserId', function(req, res, next) {
    categories.find({
      ownerId: req.body.id
    })
    .populate('rss')
    .exec(function(error, result) {
      if(error) console.log(error);
      else {
        console.log("Kategorie tego usera: " + result);
        res.json(result);
      }
    });
});

router.post('/settings/getCategoryById', function(req, res, next) {
  categories.findOne({
    _id: req.body.id
  }, function(error, result) {
     if(error) console.log(error);
     else {
       res.json(result);
     }
  });
});

router.get('*', function(req, res, next) {
  res.redirect('/');
});

module.exports = router;
