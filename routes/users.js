var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var user = require('./models/user');

function checkAuth(req, res, next) {
    if(req.session.user) {
      next();
    } else {
      res.render('index.html', { title: 'Witaj na Infamie!' });
    }
}

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});

router.post('/register', function(req, res, next) {
  console.log(req.body);
    user.findOne({
      login: req.body.login
    }, function(error, result) {
       if(error) { console.log(error); }
       else {
         if(result === null) {
           new user ({
             _id: mongoose.Types.ObjectId(),
             login: req.body.login,
             password: req.body.password,
             email: req.body.email
           }).save(function(error) {
              if(error) { console.log(error); }
              else {
                res.json({ success: "penis" });
              }
           });
         } else {
           res.json({ error: "Taki użytkownik już istnieje."});
         }
       }
    });
});

router.post('/login', function(req, res, next) {
  user.findOne({
    login: req.body.login,
    password: req.body.password
  }, function(error, result) {
     if(error) { console.log(error); }
     else {
       if(result === null) {
         console.log("Login or password is incorrect.");
         req.session.user = null;
         res.json({ error: "Wprowadzono błędne dane użytkownika. Sprawdź poprawność loginu i hasła." });
       } else {
         console.log("An user has been successfully logged in.");
         req.session.user = result._id;
         res.json({data: req.session.user });
       }
     }
  });
});

router.post('/findUserA', function(req, res, next) {
  res.json({ id: req.session.user });
});

router.post('/findUser', function(req, res, next) {
  user.findOne({
    _id: req.session.user
  })
  .populate('categories')
  .populate('rss')
  .populate('mainRSS')
  .exec(function(error, result) {
    if(error) console.log(error);
    else {
      res.json(result);
    }
  });
});

router.post('/getAllUsers', function(req, res, next) {
  user.find({},
    function(error, result) {
    if(error) console.log(error);
    else {
      if(result !== null) {
        res.json(result);
      }
    }
  });
});

router.post('/removeUser', function(req, res, next) {
  user.findOne({
    _id: req.body.userID
  }, function(error, result) {
    if(error) console.log(error);
    else {
      if(result !== null) {
        console.log(result);
        result.remove();
        res.send("OK");
      }
    }
  });
});

router.post('/searchUsers', function(req, res, next) {
  user.find({
    login: { $regex : ".*" + req.body.name + ".*" }
  }, 'login photo categories rss group', function(error, result) {
    if(error) console.log(error);
    else {
      if(result !== null) {
        console.log(result);
        res.json(result);
      }
    }
  });
});

router.post('/addCategory', function(req, res, next) {
  user.findOneAndUpdate({
    _id: req.body.ownerId
  },
  { $push: {
    categories: {
      name: req.body.categoryName,
      public: false
    }
  }},
  { safe: true, upsert: true },
  function(err, model) {
    console.log(err);
  });
});

router.post('/addRSS', function(req, res, next) {
  console.log(req.body);

  user.findOne({
    _id: req.body.ownerId,
    categories: {$elemMatch: {_id: req.body.categoryId}}

  }, function(err, result) {
    if(result) console.log(result);
  });
});

router.get('/logout', function(req, res, next) {
    req.session.destroy();
    res.writeHead(302, {
      'Location': '/'
    });
    res.end();
});

module.exports = router;
