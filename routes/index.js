var express = require('express');
var router = express.Router();

function checkAuth(req, res, next) {
    if(req.session.user) {
      res.writeHead(302, {
        'Location': '/dashboard'
        //add other headers here...
      });
      res.end();
    } else {
      next();
    }
}

/* GET home page. */
router.get('/', checkAuth, function(req, res, next) {
  res.render('index.html', { title: 'Express' });
});

module.exports = router;
