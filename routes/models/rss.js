var mongoose = require('mongoose');
var categories = require('./category');
var users = require('./user');

var rss = mongoose.model('rss', new mongoose.Schema({
    _id:        String,
    url:        String,
    name:       String,
    ownerId:    { type: String, ref: 'users' },
    categoryId: { type: String, ref: 'categories'}
}));

module.exports = rss;
