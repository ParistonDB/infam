var mongoose = require('mongoose');
var users = require('./user');
var rss = require('./rss');

var category = mongoose.model('categories', new mongoose.Schema({
    _id:     String,
    name:    String,
    ownerId: { type: String, ref: 'users' },
    rss:     [{ type: String, ref: 'rss' }]
}));

module.exports = category;
