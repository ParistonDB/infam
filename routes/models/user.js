var mongoose      = require('mongoose');
var categories    = require('./category');
var rss           = require('./rss');

var user = mongoose.model('users', new mongoose.Schema({
    _id:          String,
    login:        String,
    password:     String,
    email:        String,
    photo:        { type: String, default: 'default.png' },
    group:        { type: Number, default: 0},
    mainRSS:      { type: String, default: null, ref: 'rss'},
    categories:   [{ type: String, ref: 'categories' }],
    rss:          [{ type: String, ref: 'rss'}]
}));

module.exports = user;
