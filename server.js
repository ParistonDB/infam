var express             = require('express');
var path                = require('path');
var favicon             = require('serve-favicon');
var logger              = require('morgan');
var cookieParser        = require('cookie-parser');
var bodyParser          = require('body-parser');
var mongoose            = require('mongoose');
var session             = require('express-session');
var cors                = require('cors');

mongoose.connect('mongodb://localhost/InfamFinal', function(error) {
  if(error) {
    console.log("Blad laczenia do bazy danych: " + error);
  } else {
    console.log("Polaczona z baza danych.");
  }
});

var app = express();
app.use(cors());

var routes              = require('./routes/index');
var users               = require('./routes/users');
var dashboard           = require('./routes/dashboard');

app.use(cookieParser());
app.use(session({
    store: require ('mongoose-session')(mongoose),
    secret: 'test session',
    resave: false,
    saveUninitialized: true
}));

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.engine('html', require('ejs').renderFile);

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
//app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'node_modules')));
app.use(express.static(path.join(__dirname)));

function checkAuth(req, res, next) {
    if(req.session.user) {
      next();
    } else {
      res.writeHead(302, {
        'Location': '/'
      });
      res.end();
    }
}

app.use('/', routes);
app.use('/users', users);
app.use('/dashboard', checkAuth, dashboard);

/*
app.get('*', function(req, res, next) {
  res.render('index.html', { title: 'beniz'});
});
*/

app.listen(8080, function() {
  console.log("Nadaje na localhost:8080");
});

module.exports = app;
