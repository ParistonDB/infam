System.register(['angular2/core', 'angular2/http', 'angular2/router', './login.service', 'rxjs/add/operator/map'], function(exports_1) {
    "use strict";
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, http_1, router_1, login_service_1;
    var LoginComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (http_1_1) {
                http_1 = http_1_1;
            },
            function (router_1_1) {
                router_1 = router_1_1;
            },
            function (login_service_1_1) {
                login_service_1 = login_service_1_1;
            },
            function (_1) {}],
        execute: function() {
            LoginComponent = (function () {
                function LoginComponent(http, router, loginService) {
                    this.http = http;
                    this.router = router;
                    this.loginService = loginService;
                    this.jwt = localStorage.getItem('jwt');
                }
                LoginComponent.prototype.logIn = function (userName, userPassword) {
                    var _this = this;
                    this.loginService.logIn(userName, userPassword)
                        .subscribe(function (response) {
                        if (response.error)
                            _this.serverAnswer = response;
                        if (response.data) {
                            localStorage.setItem('jwt', JSON.stringify(response.data));
                            window.location.href = "/dashboard";
                        }
                    });
                };
                LoginComponent = __decorate([
                    core_1.Component({
                        selector: 'login',
                        templateUrl: 'components/account/login/login-form.html',
                        styleUrls: ['components/account/login/login.css'],
                        directives: [router_1.ROUTER_DIRECTIVES],
                        providers: [login_service_1.LoginService]
                    }), 
                    __metadata('design:paramtypes', [http_1.Http, router_1.Router, login_service_1.LoginService])
                ], LoginComponent);
                return LoginComponent;
            }());
            exports_1("LoginComponent", LoginComponent);
        }
    }
});
