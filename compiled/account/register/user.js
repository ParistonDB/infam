System.register([], function(exports_1) {
    var User;
    return {
        setters:[],
        execute: function() {
            User = (function () {
                function User(login, password, email) {
                    this.login = login;
                    this.password = password;
                    this.email = email;
                }
                return User;
            })();
            exports_1("User", User);
        }
    }
});
