System.register(['angular2/core', './register.service', 'angular2/router', './user', 'rxjs/add/operator/map'], function(exports_1) {
    "use strict";
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, register_service_1, router_1, user_1;
    var RegisterComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (register_service_1_1) {
                register_service_1 = register_service_1_1;
            },
            function (router_1_1) {
                router_1 = router_1_1;
            },
            function (user_1_1) {
                user_1 = user_1_1;
            },
            function (_1) {}],
        execute: function() {
            RegisterComponent = (function () {
                function RegisterComponent(registerService) {
                    this.registerService = registerService;
                }
                RegisterComponent.prototype.onSubmit = function (userName, userPassword, userEmail) {
                    var _this = this;
                    this.registerService.addUser(new user_1.User(userName, userPassword, userEmail))
                        .subscribe(function (response) {
                        if (response.success)
                            window.location.href = "/";
                        if (response.error)
                            _this.serverAnswer = response;
                    });
                };
                RegisterComponent = __decorate([
                    core_1.Component({
                        selector: 'register',
                        templateUrl: 'components/account/register/register-form.html',
                        styleUrls: ['components/account/register/register.css'],
                        directives: [router_1.ROUTER_DIRECTIVES],
                        providers: [register_service_1.RegisterService]
                    }), 
                    __metadata('design:paramtypes', [register_service_1.RegisterService])
                ], RegisterComponent);
                return RegisterComponent;
            }());
            exports_1("RegisterComponent", RegisterComponent);
        }
    }
});
