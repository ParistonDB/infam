System.register(['angular2/core', '../user/user.service'], function(exports_1) {
    "use strict";
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, user_service_1;
    var AdminComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (user_service_1_1) {
                user_service_1 = user_service_1_1;
            }],
        execute: function() {
            AdminComponent = (function () {
                function AdminComponent(userService) {
                    var _this = this;
                    this.userService = userService;
                    //getting user's information
                    this.userService.getUserByID()
                        .subscribe(function (response) {
                        if (response)
                            _this.user = response;
                        _this.jwt = response._id;
                    });
                    //getting all users
                    this.userService.getAllUsers()
                        .subscribe(function (response) {
                        if (response)
                            _this.users = response;
                    });
                }
                AdminComponent.prototype.removeUser = function (userID) {
                    console.log(userID);
                    this.userService.removeUser(userID);
                };
                AdminComponent = __decorate([
                    core_1.Component({
                        selector: 'admin',
                        templateUrl: 'components/dashboard/admin/admin.html',
                        styleUrls: ['components/dashboard/admin/admin.css']
                    }), 
                    __metadata('design:paramtypes', [user_service_1.UserService])
                ], AdminComponent);
                return AdminComponent;
            }());
            exports_1("AdminComponent", AdminComponent);
        }
    }
});
