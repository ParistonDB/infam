System.register(['angular2/core', 'angular2/router', './user/user.service', './settings/settings.component', './Home/home.component', './categories/categories.comoponent', './search/search.component', './admin/admin.component', 'rxjs/add/operator/map'], function(exports_1) {
    "use strict";
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, router_1, user_service_1, settings_component_1, home_component_1, categories_comoponent_1, search_component_1, admin_component_1;
    var DashboardComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (router_1_1) {
                router_1 = router_1_1;
            },
            function (user_service_1_1) {
                user_service_1 = user_service_1_1;
            },
            function (settings_component_1_1) {
                settings_component_1 = settings_component_1_1;
            },
            function (home_component_1_1) {
                home_component_1 = home_component_1_1;
            },
            function (categories_comoponent_1_1) {
                categories_comoponent_1 = categories_comoponent_1_1;
            },
            function (search_component_1_1) {
                search_component_1 = search_component_1_1;
            },
            function (admin_component_1_1) {
                admin_component_1 = admin_component_1_1;
            },
            function (_1) {}],
        execute: function() {
            DashboardComponent = (function () {
                function DashboardComponent(userService, router) {
                    var _this = this;
                    this.userService = userService;
                    this.router = router;
                    //getting user's information
                    this.userService.getUserByID()
                        .subscribe(function (response) {
                        if (response)
                            _this.user = response;
                        console.log(_this.user);
                        _this.jwt = response._id;
                    });
                }
                DashboardComponent.prototype.search = function (searchValue) {
                    this.router.navigate(['Search', { string: searchValue }]);
                };
                DashboardComponent.prototype.logout = function () { this.userService.logout(); };
                DashboardComponent = __decorate([
                    router_1.RouteConfig([
                        {
                            path: '/',
                            name: 'Home',
                            component: home_component_1.HomeComponent,
                            useAsDefault: true
                        },
                        {
                            path: '/dashboard/settings',
                            name: 'Settings',
                            component: settings_component_1.SettingsComponent
                        },
                        {
                            path: '/dashboard/category/:id',
                            name: 'Category',
                            component: categories_comoponent_1.CategoriesComponent
                        },
                        {
                            path: '/dashboard/search/:string',
                            name: 'Search',
                            component: search_component_1.SearchComponent
                        },
                        {
                            path: '/dashboard/admin',
                            name: 'Admin',
                            component: admin_component_1.AdminComponent
                        }
                    ]),
                    core_1.Component({
                        selector: 'dashboard',
                        templateUrl: 'components/dashboard/dashboard.html',
                        styleUrls: ['components/dashboard/dashboard.css'],
                        directives: [router_1.ROUTER_DIRECTIVES, settings_component_1.SettingsComponent, search_component_1.SearchComponent],
                        providers: [user_service_1.UserService]
                    }), 
                    __metadata('design:paramtypes', [user_service_1.UserService, router_1.Router])
                ], DashboardComponent);
                return DashboardComponent;
            }());
            exports_1("DashboardComponent", DashboardComponent);
        }
    }
});
