System.register(['angular2/core', 'angular2/router', './settings.service', '../user/user.service', 'angular2/http'], function(exports_1) {
    "use strict";
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, router_1, settings_service_1, user_service_1, http_1;
    var EscapeHtmlPipe, SettingsComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (router_1_1) {
                router_1 = router_1_1;
            },
            function (settings_service_1_1) {
                settings_service_1 = settings_service_1_1;
            },
            function (user_service_1_1) {
                user_service_1 = user_service_1_1;
            },
            function (http_1_1) {
                http_1 = http_1_1;
            }],
        execute: function() {
            EscapeHtmlPipe = (function () {
                function EscapeHtmlPipe() {
                }
                EscapeHtmlPipe.prototype.transform = function (value, args) {
                    // We don't like bold text, it's dangerous! Remove it!
                    if (args === void 0) { args = []; }
                    // Naive detection!
                    if (value.indexOf('<b>') != -1) {
                        return value.replace('<b>', '').replace('<\/b>', '');
                    }
                };
                EscapeHtmlPipe = __decorate([
                    core_1.Pipe({ name: 'escapeHtml', pure: false }), 
                    __metadata('design:paramtypes', [])
                ], EscapeHtmlPipe);
                return EscapeHtmlPipe;
            }());
            SettingsComponent = (function () {
                function SettingsComponent(http, settingsService, userService) {
                    var _this = this;
                    this.http = http;
                    this.settingsService = settingsService;
                    this.userService = userService;
                    userService.getUserByID()
                        .subscribe(function (response) {
                        if (response)
                            _this.user = response;
                        _this.rss = response.categories;
                        _this.jwt = response._id;
                        settingsService.getCategoriesByUserId(response._id)
                            .subscribe(function (response) {
                            if (response)
                                _this.categories = response;
                        });
                    });
                }
                SettingsComponent.prototype.addMainRSS = function (rssID) {
                    this.settingsService.addMainRSS(rssID, this.jwt);
                };
                SettingsComponent.prototype.editPhoto = function (photoName) {
                    this.settingsService.editPhoto(photoName, this.jwt);
                };
                SettingsComponent.prototype.editPassword = function (oldPassword, newPassword) {
                    this.settingsService.editPassword(oldPassword, newPassword, this.jwt);
                };
                SettingsComponent.prototype.addCategory = function (categoryName) {
                    this.settingsService.addCategory(categoryName, this.jwt);
                };
                SettingsComponent.prototype.editCategory = function (categoryId, categoryNewName) {
                    this.settingsService.editCategory(categoryId, categoryNewName);
                };
                SettingsComponent.prototype.removeCategory = function (categoryId) {
                    this.settingsService.removeCategory(categoryId);
                };
                SettingsComponent.prototype.addRSS = function (url, name, categoryId) {
                    this.settingsService.addRSS(url, this.jwt, name, categoryId);
                };
                SettingsComponent.prototype.editRSS = function (RSSId, newName) {
                    this.settingsService.editRSS(RSSId, newName);
                };
                SettingsComponent.prototype.removeRSS = function (RSSId) {
                    this.settingsService.removeRSS(RSSId);
                };
                SettingsComponent = __decorate([
                    core_1.Component({
                        selector: 'settings',
                        templateUrl: 'components/dashboard/settings/settings.html',
                        directives: [router_1.ROUTER_DIRECTIVES],
                        providers: [settings_service_1.SettingsService],
                        styleUrls: ['components/dashboard/settings/settings.css']
                    }), 
                    __metadata('design:paramtypes', [http_1.Http, settings_service_1.SettingsService, user_service_1.UserService])
                ], SettingsComponent);
                return SettingsComponent;
            }());
            exports_1("SettingsComponent", SettingsComponent);
        }
    }
});
