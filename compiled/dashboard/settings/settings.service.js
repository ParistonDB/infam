System.register(['angular2/http', 'angular2/core'], function(exports_1) {
    "use strict";
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var http_1, core_1;
    var SettingsService;
    return {
        setters:[
            function (http_1_1) {
                http_1 = http_1_1;
            },
            function (core_1_1) {
                core_1 = core_1_1;
            }],
        execute: function() {
            SettingsService = (function () {
                function SettingsService(http) {
                    this.http = http;
                }
                SettingsService.prototype.addMainRSS = function (rssID, userID) {
                    var headers = new http_1.Headers();
                    headers.append('Content-type', 'application/json');
                    var rss = JSON.stringify({
                        rssID: rssID,
                        userID: userID
                    });
                    this.http.post('/dashboard/settings/addMainRSS', rss, { headers: headers })
                        .subscribe(function (response) {
                        if (response)
                            window.location.href = "/dashboard/settings";
                    });
                };
                SettingsService.prototype.editPhoto = function (photoName, userID) {
                    var headers = new http_1.Headers();
                    headers.append('Content-type', 'application/json');
                    var user = JSON.stringify({
                        userID: userID,
                        photoName: photoName
                    });
                    this.http.post('/dashboard/settings/editPhoto', user, { headers: headers })
                        .subscribe(function (response) {
                        if (response)
                            window.location.href = "/dashboard/settings";
                    });
                };
                SettingsService.prototype.editPassword = function (oldPassword, newPassword, userID) {
                    var headers = new http_1.Headers();
                    headers.append('Content-type', 'application/json');
                    var user = JSON.stringify({
                        userID: userID,
                        oldPassword: oldPassword,
                        newPassword: newPassword
                    });
                    this.http.post('/dashboard/settings/editPassword', user, { headers: headers })
                        .subscribe(function (response) {
                        if (response)
                            window.location.href = "/dashboard/settings";
                    });
                };
                SettingsService.prototype.addCategory = function (categoryName, ownerId) {
                    var headers = new http_1.Headers();
                    headers.append('Content-type', 'application/json');
                    var category = JSON.stringify({
                        ownerId: ownerId,
                        categoryName: categoryName
                    });
                    this.http.post('/dashboard/settings/addCategory', category, { headers: headers })
                        .subscribe(function (response) {
                        if (response)
                            window.location.href = "/dashboard/settings";
                    });
                };
                SettingsService.prototype.editCategory = function (categoryId, categoryNewName) {
                    var headers = new http_1.Headers();
                    headers.append('Content-type', 'application/json');
                    var category = JSON.stringify({
                        categoryId: categoryId,
                        newName: categoryNewName
                    });
                    this.http.post('/dashboard/settings/editCategory', category, { headers: headers })
                        .subscribe(function (response) {
                        if (response)
                            window.location.href = "/dashboard/settings";
                    });
                };
                SettingsService.prototype.removeCategory = function (categoryId) {
                    var headers = new http_1.Headers();
                    headers.append('Content-type', 'application/json');
                    var category = JSON.stringify({
                        categoryId: categoryId,
                    });
                    this.http.post('/dashboard/settings/removeCategory', category, { headers: headers })
                        .subscribe(function (response) {
                        if (response)
                            window.location.href = "/dashboard/settings";
                    });
                };
                SettingsService.prototype.getCategoriesByUserId = function (id) {
                    var headers = new http_1.Headers();
                    headers.append('Content-type', 'application/json');
                    return this.http.post('/dashboard/settings/getCategoriesByUserId', JSON.stringify({ id: id }), { headers: headers })
                        .map(function (response) { return response.json(); });
                };
                SettingsService.prototype.getCategoryById = function (id) {
                    var headers = new http_1.Headers();
                    headers.append('Content-type', 'application/json');
                    return this.http.post('/dashboard/settings/getCategoryById', JSON.stringify({ id: id }), { headers: headers })
                        .map(function (response) { return response.json(); });
                };
                SettingsService.prototype.addRSS = function (url, ownerId, name, categoryId) {
                    var headers = new http_1.Headers();
                    headers.append('Content-type', 'application/json');
                    var rss = JSON.stringify({
                        ownerId: ownerId,
                        categoryId: categoryId,
                        name: name,
                        url: url
                    });
                    this.http.post('/dashboard/settings/addRSS', rss, { headers: headers })
                        .subscribe();
                };
                SettingsService.prototype.editRSS = function (id, newName) {
                    var headers = new http_1.Headers();
                    headers.append('Content-type', 'application/json');
                    var rss = JSON.stringify({
                        id: id,
                        newName: newName,
                    });
                    this.http.post('/dashboard/settings/editRSS', rss, { headers: headers })
                        .subscribe(function (response) {
                        if (response)
                            window.location.href = "/dashboard/settings";
                    });
                };
                SettingsService.prototype.removeRSS = function (id) {
                    var headers = new http_1.Headers();
                    headers.append('Content-type', 'application/json');
                    var rss = JSON.stringify({
                        id: id,
                    });
                    this.http.post('/dashboard/settings/removeRSS', rss, { headers: headers })
                        .subscribe(function (response) {
                        if (response)
                            window.location.href = "/dashboard/settings";
                    });
                };
                SettingsService.prototype.getArticles = function (rssUrl) {
                    var headers = new http_1.Headers();
                    headers.append('Content-type', 'application/rss+xml');
                    return this.http.get(rssUrl)
                        .map(function (response) { return response.text(); });
                };
                SettingsService = __decorate([
                    core_1.Injectable(), 
                    __metadata('design:paramtypes', [http_1.Http])
                ], SettingsService);
                return SettingsService;
            }());
            exports_1("SettingsService", SettingsService);
        }
    }
});
