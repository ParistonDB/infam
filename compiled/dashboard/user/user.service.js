System.register(['angular2/http', 'angular2/core'], function(exports_1) {
    "use strict";
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var http_1, core_1;
    var UserService;
    return {
        setters:[
            function (http_1_1) {
                http_1 = http_1_1;
            },
            function (core_1_1) {
                core_1 = core_1_1;
            }],
        execute: function() {
            UserService = (function () {
                function UserService(http) {
                    this.http = http;
                }
                UserService.prototype.logout = function () {
                    localStorage.removeItem('jwt');
                    window.location.href = "/users/logout";
                };
                UserService.prototype.removeUser = function (userID) {
                    var headers = new http_1.Headers();
                    headers.append('Content-type', 'application/json');
                    var Find = JSON.stringify({
                        userID: userID
                    });
                    return this.http.post('/users/removeUser', Find, { headers: headers })
                        .subscribe(function (response) {
                        if (response)
                            if (response)
                                window.location.href = "/dashboard/admin";
                    });
                };
                UserService.prototype.searchUsers = function (name) {
                    var headers = new http_1.Headers();
                    headers.append('Content-type', 'application/json');
                    var Find = JSON.stringify({
                        name: name
                    });
                    return this.http.post('/users/searchUsers', Find, { headers: headers })
                        .map(function (response) { return response.json(); });
                };
                UserService.prototype.getAllUsers = function () {
                    var headers = new http_1.Headers();
                    headers.append('Content-type', 'application/json');
                    var Find = JSON.stringify({});
                    return this.http.post('/users/getAllUsers', Find, { headers: headers })
                        .map(function (response) { return response.json(); });
                };
                //returns all user information + categories
                UserService.prototype.getUserByID = function () {
                    var headers = new http_1.Headers();
                    headers.append('Content-type', 'application/json');
                    var Find = JSON.stringify({});
                    return this.http.post('/users/findUser', Find, { headers: headers })
                        .map(function (response) { return response.json(); });
                };
                UserService = __decorate([
                    core_1.Injectable(), 
                    __metadata('design:paramtypes', [http_1.Http])
                ], UserService);
                return UserService;
            }());
            exports_1("UserService", UserService);
        }
    }
});
