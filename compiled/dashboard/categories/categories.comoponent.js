System.register(['angular2/core', '../settings/settings.service', '../user/user.service', 'angular2/router', 'angular2/http'], function(exports_1) {
    "use strict";
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, settings_service_1, user_service_1, router_1, http_1;
    var CategoriesComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (settings_service_1_1) {
                settings_service_1 = settings_service_1_1;
            },
            function (user_service_1_1) {
                user_service_1 = user_service_1_1;
            },
            function (router_1_1) {
                router_1 = router_1_1;
            },
            function (http_1_1) {
                http_1 = http_1_1;
            }],
        execute: function() {
            CategoriesComponent = (function () {
                function CategoriesComponent(http, userService, settingsService, params) {
                    var _this = this;
                    this.http = http;
                    this.userService = userService;
                    this.settingsService = settingsService;
                    this.id = params.get('id');
                    //getting user's information
                    this.userService.getUserByID()
                        .subscribe(function (response) {
                        if (response)
                            _this.user = response;
                        _this.jwt = response._id;
                    });
                    //getting category
                    this.settingsService.getCategoryById(this.id)
                        .subscribe(function (response) { if (response)
                        _this.category = response; });
                }
                CategoriesComponent.prototype.bierRSS = function (rssUrl) {
                    var _this = this;
                    this.settingsService.getArticles(rssUrl)
                        .subscribe(function (response) {
                        var headers = new http_1.Headers();
                        headers.append('Content-type', 'application/json');
                        var article = JSON.stringify({
                            tresc: response
                        });
                        _this.http.post('/dashboard/settings/articles', article, { headers: headers })
                            .subscribe(function (response) {
                            console.log(response.json());
                            _this.articles = response.json();
                        });
                    });
                };
                CategoriesComponent = __decorate([
                    core_1.Component({
                        selector: 'categories',
                        templateUrl: 'components/dashboard/categories/categories.html',
                        styleUrls: ['components/dashboard/categories/categories.css'],
                        providers: [settings_service_1.SettingsService]
                    }), 
                    __metadata('design:paramtypes', [http_1.Http, user_service_1.UserService, settings_service_1.SettingsService, router_1.RouteParams])
                ], CategoriesComponent);
                return CategoriesComponent;
            }());
            exports_1("CategoriesComponent", CategoriesComponent);
        }
    }
});
