System.register(['angular2/core', '../user/user.service', 'angular2/router', '../settings/settings.service', 'angular2/http'], function(exports_1) {
    "use strict";
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, user_service_1, router_1, settings_service_1, http_1;
    var HomeComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (user_service_1_1) {
                user_service_1 = user_service_1_1;
            },
            function (router_1_1) {
                router_1 = router_1_1;
            },
            function (settings_service_1_1) {
                settings_service_1 = settings_service_1_1;
            },
            function (http_1_1) {
                http_1 = http_1_1;
            }],
        execute: function() {
            HomeComponent = (function () {
                function HomeComponent(userService, settingsService, http) {
                    var _this = this;
                    this.userService = userService;
                    this.settingsService = settingsService;
                    this.http = http;
                    this.userService.getUserByID()
                        .subscribe(function (response) {
                        if (response) {
                            _this.user = response;
                            _this.jwt = response._id;
                            if (response.mainRSS) {
                                _this.settingsService.getArticles(response.mainRSS.url)
                                    .subscribe(function (response) {
                                    var headers = new http_1.Headers();
                                    headers.append('Content-type', 'application/json');
                                    var article = JSON.stringify({
                                        tresc: response
                                    });
                                    _this.http.post('/dashboard/settings/articles', article, { headers: headers })
                                        .subscribe(function (response) {
                                        _this.articles = response.json();
                                        console.log(_this.articles);
                                    });
                                });
                            }
                        }
                    });
                }
                HomeComponent = __decorate([
                    core_1.Component({
                        selector: 'home',
                        templateUrl: 'components/dashboard/Home/home.html',
                        styleUrls: ['components/dashboard/Home/home.css'],
                        directives: [router_1.ROUTER_DIRECTIVES],
                        providers: [settings_service_1.SettingsService]
                    }), 
                    __metadata('design:paramtypes', [user_service_1.UserService, settings_service_1.SettingsService, http_1.Http])
                ], HomeComponent);
                return HomeComponent;
            }());
            exports_1("HomeComponent", HomeComponent);
        }
    }
});
