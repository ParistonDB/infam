export class User {
  constructor(
    login: string,
    password: string,
    email: string
  ) {
    this.login = login;
    this.password = password;
    this.email = email;
  }

  login: string;
  password: string;
  email: string;
}
