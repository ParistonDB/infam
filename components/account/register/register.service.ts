import {User} from './user'
import {Http, HTTP_PROVIDERS, Headers} from 'angular2/http'
import {Injectable} from 'angular2/core'

@Injectable()
export class RegisterService {
  constructor(private http: Http) {}

  addUser(user: User) {
    var headers = new Headers();

    headers.append('Content-type', 'application/json');

    return this.http.post('/users/register', JSON.stringify(user), {headers: headers})
    .map(response => response.json());
  }
}
