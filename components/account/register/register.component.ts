import {Component}                                      from 'angular2/core'
import {RegisterService}                                from './register.service'
import {RouteConfig, Router, ROUTER_DIRECTIVES}         from 'angular2/router'
import {User}                                           from './user'
import 'rxjs/add/operator/map';

@Component({
  selector: 'register',
  templateUrl: 'components/account/register/register-form.html',
  styleUrls: ['components/account/register/register.css'],
  directives: [ROUTER_DIRECTIVES],
  providers: [RegisterService]
})

export class RegisterComponent {
  constructor(public registerService: RegisterService) {}

  serverAnswer: Object;

  onSubmit(userName: string, userPassword: string, userEmail: string) {
    this.registerService.addUser(new User(userName, userPassword, userEmail))
    .subscribe (
        response => {
            if (response.success)   window.location.href = "/";
            if (response.error)     this.serverAnswer = response;
        }
    );
  }
}
