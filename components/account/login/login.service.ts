import {Http, HTTP_PROVIDERS, Headers} from 'angular2/http'
import {Injectable} from 'angular2/core'

@Injectable()
export class LoginService {

  constructor(private http: Http) {}

  logIn(userName: string, userPassword: string) {
    var headers = new Headers();
    headers.append('Content-type', 'application/json');

    var User = JSON.stringify({
        login: userName,
        password: userPassword,
    });

    return this.http.post('/users/login', User, {headers: headers})
    .map(response => response.json());
  }
}
