import {Component}                                     from 'angular2/core'
import {Http, HTTP_PROVIDERS, Headers}                 from 'angular2/http'
import {RouteConfig, Router, ROUTER_DIRECTIVES}        from 'angular2/router'
import {DashboardComponent}                            from '../../dashboard/dashboard.component'
import {LoginService}                                  from './login.service'
import 'rxjs/add/operator/map';

@Component({
  selector: 'login',
  templateUrl: 'components/account/login/login-form.html',
  styleUrls: ['components/account/login/login.css'],
  directives: [ROUTER_DIRECTIVES],
  providers: [LoginService]
})

export class LoginComponent {
  jwt: string;

  constructor(private http: Http, private router: Router, private loginService: LoginService) {
    this.jwt = localStorage.getItem('jwt');
  }

  serverAnswer: Object;

  logIn(userName: string, userPassword: string) {
    this.loginService.logIn(userName, userPassword)
    .subscribe (
      response => {
        if(response.error) this.serverAnswer = response;

        if(response.data) {
          localStorage.setItem('jwt', JSON.stringify(response.data));
          window.location.href = "/dashboard";
        }
      }
    )
  }
}
