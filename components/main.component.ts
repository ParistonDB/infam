import {Component}                          from 'angular2/core'
import {RegisterComponent}                  from './account/register/register.component'
import {LoginComponent}                     from './account/login/login.component'
import {DashboardComponent}                 from './dashboard/dashboard.component'
import {RouteConfig, ROUTER_DIRECTIVES}     from 'angular2/router'

@RouteConfig([
  {
    path: '/',
    name: 'Login',
    component: LoginComponent,
    useAsDefault: true
  },
  {
    path: '/login',
    name: 'SignIn',
    component: LoginComponent
  },
  {
    path: '/register',
    name: 'Registration',
    component: RegisterComponent
  }
])

@Component({
  selector: 'main-application',
  template: `
    <div class="container">
      <div class="not_logged">
        <router-outlet></router-outlet>
      </div>
    </div>
  `,
  directives: [RegisterComponent, ROUTER_DIRECTIVES]
})

export class MainComponent {

}
