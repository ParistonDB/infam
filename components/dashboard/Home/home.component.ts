import {Component}                         from 'angular2/core'
import {UserService}                       from '../user/user.service'
import {Router, ROUTER_DIRECTIVES}         from 'angular2/router';
import {SettingsService}                   from '../settings/settings.service'
import {Http, Headers} from 'angular2/http'

@Component({
  selector: 'home',
  templateUrl: 'components/dashboard/Home/home.html',
  styleUrls: ['components/dashboard/Home/home.css'],
  directives: [ROUTER_DIRECTIVES],
  providers: [SettingsService]
})

export class HomeComponent {
  jwt: string;
  user: Object;
  p: Object;
  articles: Object;

  constructor(private userService: UserService, private settingsService: SettingsService, private http: Http) {
    this.userService.getUserByID()
    .subscribe(
      response => {
        if(response)  {
          this.user = response;
          this.jwt = response._id;
          if(response.mainRSS) {
            this.settingsService.getArticles(response.mainRSS.url)
            .subscribe(
              response => {
                var headers = new Headers();
                headers.append('Content-type', 'application/json');
                var article = JSON.stringify({
                  tresc: response
                });
                this.http.post('/dashboard/settings/articles', article, {headers: headers})
                .subscribe(
                  response => {
                    this.articles = response.json();
                    console.log(this.articles);
                  }
                )
              }
            )
          }
        }
      }
    );
  }
}
