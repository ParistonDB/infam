import {Http, HTTP_PROVIDERS, Headers} from 'angular2/http'
import {Injectable} from 'angular2/core'

@Injectable()
export class SettingsService {
  constructor(private http: Http) {}

  articles: Object;

  addMainRSS(rssID: string, userID: string) {
    var headers = new Headers();
    headers.append('Content-type', 'application/json');

    var rss = JSON.stringify({
        rssID: rssID,
        userID: userID
    });

    this.http.post('/dashboard/settings/addMainRSS', rss, {headers: headers})
    .subscribe(
      response => {
        if(response) window.location.href = "/dashboard/settings";
      }
    );
  }

  editPhoto(photoName: string, userID: string) {
    var headers = new Headers();
    headers.append('Content-type', 'application/json');

    var user = JSON.stringify({
        userID: userID,
        photoName: photoName
    });

    this.http.post('/dashboard/settings/editPhoto', user, {headers: headers})
    .subscribe(
      response => {
        if(response) window.location.href = "/dashboard/settings";
      }
    );
  }

  editPassword(oldPassword: string, newPassword: string, userID: string) {
    var headers = new Headers();
    headers.append('Content-type', 'application/json');

    var user = JSON.stringify({
        userID: userID,
        oldPassword: oldPassword,
        newPassword: newPassword
    });

    this.http.post('/dashboard/settings/editPassword', user, {headers: headers})
    .subscribe(
      response => {
        if(response) window.location.href = "/dashboard/settings";
      }
    );
  }

  addCategory(categoryName: string, ownerId: string) {
    var headers = new Headers();
    headers.append('Content-type', 'application/json');

    var category = JSON.stringify({
        ownerId: ownerId,
        categoryName: categoryName
    });

    this.http.post('/dashboard/settings/addCategory', category, {headers: headers})
    .subscribe(
      response => {
        if(response) window.location.href = "/dashboard/settings";
      }
    );
  }

  editCategory(categoryId: string, categoryNewName: string) {
    var headers = new Headers();
    headers.append('Content-type', 'application/json');

    var category = JSON.stringify({
        categoryId: categoryId,
        newName: categoryNewName
    });

    this.http.post('/dashboard/settings/editCategory', category, {headers: headers})
    .subscribe(
      response => {
        if(response) window.location.href = "/dashboard/settings";
      }
    );
  }

  removeCategory(categoryId: string) {
    var headers = new Headers();
    headers.append('Content-type', 'application/json');

    var category = JSON.stringify({
        categoryId: categoryId,
    });

    this.http.post('/dashboard/settings/removeCategory', category, {headers: headers})
    .subscribe(
      response => {
        if(response) window.location.href = "/dashboard/settings";
      }
    );
  }

  getCategoriesByUserId(id: string) {
    var headers = new Headers();
    headers.append('Content-type', 'application/json');

    return this.http.post('/dashboard/settings/getCategoriesByUserId', JSON.stringify({ id: id }), {headers: headers})
    .map(response => response.json());
  }

  getCategoryById(id: string) {
      var headers = new Headers();
      headers.append('Content-type', 'application/json');

      return this.http.post('/dashboard/settings/getCategoryById', JSON.stringify({ id: id }), {headers: headers})
      .map(response => response.json());
  }

  addRSS(url: string, ownerId: string, name: string, categoryId: string) {
    var headers = new Headers();
    headers.append('Content-type', 'application/json');

    var rss = JSON.stringify({
        ownerId: ownerId,
        categoryId: categoryId,
        name: name,
        url: url
    });

    this.http.post('/dashboard/settings/addRSS', rss, {headers: headers})
    .subscribe();
  }

  editRSS(id: string, newName: string) {
    var headers = new Headers();
    headers.append('Content-type', 'application/json');

    var rss = JSON.stringify({
        id: id,
        newName: newName,
    });

    this.http.post('/dashboard/settings/editRSS', rss, {headers: headers})
    .subscribe(
      response => {
        if(response) window.location.href = "/dashboard/settings";
      }
    );
  }

  removeRSS(id: string) {
    var headers = new Headers();
    headers.append('Content-type', 'application/json');

    var rss = JSON.stringify({
        id: id,
    });

    this.http.post('/dashboard/settings/removeRSS', rss, {headers: headers})
    .subscribe(
      response => {
        if(response) window.location.href = "/dashboard/settings";
      }
    );
  }

  getArticles(rssUrl: string) {
    var headers = new Headers();
    headers.append('Content-type', 'application/rss+xml');

    return this.http.get(rssUrl)
    .map(response => response.text());
  }
}
