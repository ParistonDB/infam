import {Component, Pipe, PipeTransform}                          from 'angular2/core'
import {Router, ROUTER_DIRECTIVES}          from 'angular2/router';
import {SettingsService}                    from './settings.service'
import {UserService}                        from '../user/user.service'
import {Headers, Http}                            from 'angular2/http'

@Pipe({name: 'escapeHtml', pure: false})
class EscapeHtmlPipe implements PipeTransform {
   transform(value: any, args: any[] = []) {
        // We don't like bold text, it's dangerous! Remove it!

        // Naive detection!
        if(value.indexOf('<b>') != -1) {
          return value.replace('<b>', '').replace('<\/b>', '');
        }
   }
}


@Component({
  selector:     'settings',
  templateUrl:  'components/dashboard/settings/settings.html',
  directives:   [ROUTER_DIRECTIVES],
  providers:    [SettingsService],
  styleUrls:    ['components/dashboard/settings/settings.css']
})

export class SettingsComponent {
    jwt: string;
    user: Object;
    rss: Object;
    articles: Object;
    categoryOfRSSId: Object;
    categories: Object;

    constructor(private http: Http, private settingsService: SettingsService, private userService: UserService) {
      userService.getUserByID()
      .subscribe(
        response => {
          if(response) this.user = response;
          this.rss = response.categories;
          this.jwt = response._id;
          settingsService.getCategoriesByUserId(response._id)
          .subscribe(
            response => {
              if(response) this.categories = response;
            }
          )
        }
      );
    }

    addMainRSS(rssID: string) {
        this.settingsService.addMainRSS(rssID, this.jwt);
    }

    editPhoto(photoName: string) {
      this.settingsService.editPhoto(photoName, this.jwt);
    }

    editPassword(oldPassword: string, newPassword: string) {
      this.settingsService.editPassword(oldPassword, newPassword, this.jwt);
    }

    addCategory(categoryName: string) {
      this.settingsService.addCategory(categoryName, this.jwt);
    }

    editCategory(categoryId: string, categoryNewName: string) {
      this.settingsService.editCategory(categoryId, categoryNewName);
    }

    removeCategory(categoryId: string) {
      this.settingsService.removeCategory(categoryId);
    }

    addRSS(url: string, name: string, categoryId: string) {
      this.settingsService.addRSS(url, this.jwt, name, categoryId);
    }

    editRSS(RSSId: string, newName: string) {
      this.settingsService.editRSS(RSSId, newName);
    }

    removeRSS(RSSId: string) {
      this.settingsService.removeRSS(RSSId);
    }
}
