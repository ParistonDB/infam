import {Component}                                      from 'angular2/core'
import {RouteConfig, ROUTER_DIRECTIVES, Router}         from 'angular2/router';
import {UserService}                                    from './user/user.service'
import {Http, HTTP_PROVIDERS, Headers}                  from 'angular2/http'
import {SettingsComponent}                              from './settings/settings.component'
import {HomeComponent}                                  from './Home/home.component'
import {CategoriesComponent}                            from './categories/categories.comoponent'
import {SearchComponent}                                from './search/search.component'
import {AdminComponent}                                 from './admin/admin.component'

import 'rxjs/add/operator/map';

@RouteConfig([
  {
    path: '/',
    name: 'Home',
    component: HomeComponent,
    useAsDefault: true
  },
  {
    path: '/dashboard/settings',
    name: 'Settings',
    component: SettingsComponent
  },
  {
    path: '/dashboard/category/:id',
    name: 'Category',
    component: CategoriesComponent
  },
  {
    path: '/dashboard/search/:string',
    name: 'Search',
    component: SearchComponent
  },
  {
    path: '/dashboard/admin',
    name: 'Admin',
    component: AdminComponent
  }
])

@Component({
  selector: 'dashboard',
  templateUrl: 'components/dashboard/dashboard.html',
  styleUrls: ['components/dashboard/dashboard.css'],
  directives: [ROUTER_DIRECTIVES, SettingsComponent, SearchComponent],
  providers: [UserService]
})

export class DashboardComponent {

  jwt: string;
  user: Object;

  constructor(private userService: UserService, private router: Router) {
    //getting user's information
    this.userService.getUserByID()
    .subscribe(
      response => {
        if(response) this.user = response;
        console.log(this.user);
        this.jwt = response._id;
      }
    );
  }

  search(searchValue: string) {
    this.router.navigate(['Search', {string: searchValue}]);
  }

  logout() { this.userService.logout() }
}
