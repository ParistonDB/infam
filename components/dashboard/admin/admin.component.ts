import {Component} from 'angular2/core'
import {UserService}                                    from '../user/user.service'
import {SettingsComponent}                              from '../settings/settings.component'

@Component({
  selector: 'admin',
  templateUrl: 'components/dashboard/admin/admin.html',
  styleUrls: ['components/dashboard/admin/admin.css']
})

export class AdminComponent {
  jwt: string;
  user: Object;
  users: Object;

  constructor(private userService: UserService) {
    //getting user's information
    this.userService.getUserByID()
    .subscribe(
      response => {
        if(response) this.user = response;
        this.jwt = response._id;
      }
    );
    //getting all users
    this.userService.getAllUsers()
    .subscribe(
      response => {
        if(response) this.users = response;
      }
    );
  }

  removeUser(userID: string) {
    console.log(userID);
    this.userService.removeUser(userID);
  }
}
