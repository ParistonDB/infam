import {Component} from 'angular2/core'
import {SettingsService} from '../settings/settings.service'
import {UserService} from '../user/user.service'
import {RouteParams} from 'angular2/router';
import {Http, Headers} from 'angular2/http'

@Component({
  selector: 'categories',
  templateUrl: 'components/dashboard/categories/categories.html',
  styleUrls: ['components/dashboard/categories/categories.css'],
  providers: [SettingsService]
})

export class CategoriesComponent {
  jwt: string;
  id: string;
  user: Object;
  category: Object;
  articles: Object;

  constructor(private http: Http, private userService: UserService, private settingsService: SettingsService, params: RouteParams) {
    this.id = params.get('id');

    //getting user's information
    this.userService.getUserByID()
    .subscribe(
      response => {
        if(response) this.user = response;
        this.jwt = response._id;
      }
    );

    //getting category
    this.settingsService.getCategoryById(this.id)
    .subscribe(response => { if(response) this.category = response; })
  }

  bierRSS(rssUrl: string) {
    this.settingsService.getArticles(rssUrl)
    .subscribe(
      response => {
        var headers = new Headers();
        headers.append('Content-type', 'application/json');
        var article = JSON.stringify({
          tresc: response
        });
        this.http.post('/dashboard/settings/articles', article, {headers: headers})

        .subscribe(
          response => {
            console.log(response.json());

            this.articles = response.json();
          }
        )
      }
    )
  }
}
