import {Http, HTTP_PROVIDERS, Headers} from 'angular2/http'
import {Injectable} from 'angular2/core'

@Injectable()
export class UserService {
  constructor(private http: Http) {}

  logout() {
      localStorage.removeItem('jwt');
      window.location.href = "/users/logout";
  }

  removeUser(userID: string) {
    var headers = new Headers();
    headers.append('Content-type', 'application/json');

    var Find = JSON.stringify({
      userID: userID
    });

    return this.http.post('/users/removeUser', Find, {headers: headers})
    .subscribe(
      response => {
        if(response) if(response) window.location.href = "/dashboard/admin";
      }
    );
  }

  searchUsers(name: string) {
    var headers = new Headers();
    headers.append('Content-type', 'application/json');

    var Find = JSON.stringify({
      name: name
    });

    return this.http.post('/users/searchUsers', Find, {headers: headers})
    .map(response => response.json());
  }

  getAllUsers() {
    var headers = new Headers();

    headers.append('Content-type', 'application/json');
    var Find = JSON.stringify({

    });

    return this.http.post('/users/getAllUsers', Find, {headers: headers})
    .map(response => response.json());
  }

  //returns all user information + categories
  getUserByID() {
    var headers = new Headers();

    headers.append('Content-type', 'application/json');
    var Find = JSON.stringify({

    });

    return this.http.post('/users/findUser', Find, {headers: headers})
    .map(response => response.json());
  }
}
