import {Component} from 'angular2/core'
import {UserService} from '../user/user.service'
import {RouteParams} from 'angular2/router';

@Component({
  selector: 'search-result',
  templateUrl: 'components/dashboard/search/search.html',
  styleUrls: ['components/dashboard/search/search.css'],
  providers: [UserService]
})

export class SearchComponent {
  searchResult: Object;

  constructor(private userService: UserService, params: RouteParams) {
    console.log(params.get('string'));
    this.userService.searchUsers(params.get('string'))
    .subscribe(
      response => {
        if(response) this.searchResult = response;
      }
    )
  }
}
